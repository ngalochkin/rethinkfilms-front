var express = require('express');
var rethink = require('rethinkdb');
var fs    = require('fs');
var app = express();

var limit = 20;
var offset = 0;

var root_path = 'public';
var index_file = '/index.html';

app.use(express.static(__dirname + '/' + root_path));
app.use(require('connect-livereload')({
    port: 80
}));

app.get('/random', function (req, res) {
    rethink.connect({host: 'localhost', port: 28015}, function (err, conn) {
        if (!err) {
            rethink.db('rethinkfilms_development').table('items').sample(1).run(conn, function (err, result) {
                if (err) throw err;

                res.set('Content-Type', 'application/json');
                res.send(JSON.stringify(result, null, 2));
            });
        } else {
            console.log(err);
        }
    });
});

app.get('/grid/limit/:limit/offset/:offset', function (req, res) {

    offset = parseInt(req.params.offset);
    limit = parseInt(req.params.limit);

    rethink.connect({host: 'localhost', port: 28015}, function (err, conn) {
        if (!err) {
            rethink.db('rethinkfilms_development').table('items').orderBy('id').skip(offset).limit(limit).run(conn, function (err, result) {
                if (err) throw err;

                res.set('Content-Type', 'application/json');
                res.send(JSON.stringify(result, null, 2));
            });
        } else {
            console.log(err);
        }
    });
});

app.get('/item/:item_id', function (req, res) {

    item_id = req.params.item_id;

    rethink.connect({host: 'localhost', port: 28015}, function (err, conn) {
        if (!err) {
            rethink.db('rethinkfilms_development').table('items').orderBy('id').filter({id: item_id}).run(conn, function (err, result) {
                if (err) throw err;

                res.set('Content-Type', 'application/json');
                res.send(JSON.stringify(result, null, 2));
            });
        } else {
            console.log(err);
        }
    });
});

app.get('*', function (req, res) {
    fs.open(root_path + index_file, 'r', function (err, fileToRead) {
        if (!err) {
            fs.readFile(root_path + index_file, {encoding: 'utf-8'}, function (err, data) {
                if (!err) {
                    res.writeHead(200, {'Content-Type': 'text/html'});
                    res.write(data);
                    res.end();
                } else {
                    console.log(err);
                }
            });
        } else {
            console.log(err);
        }
    });
});

app.listen(80);