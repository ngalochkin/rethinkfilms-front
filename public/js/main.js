require.config({
    baseUrl: "/js/",
    paths: {
        "text": "lib/text",
        "react": "lib/react.min",
        "JSXTransformer": "lib/JSXTransformer"
    },
    shim: {
        'JSXTransformer': {
            deps: ["react"]
        }
    },
    jsx: {
        fileExtension: '.js'
    }

})

require([
    'jsx!views/item'
])