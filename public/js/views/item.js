define([
    "jquery",
    "react"
], function($,React) {

    function Lazy(item) {
        loadImage(item);
        function loadImage(container) {
            var src = container.getAttribute('data-img'),
                handlerImage = new Image();

            handlerImage.src = src;
            handlerImage.container = container;
            handlerImage.onload = imageDone;
        };

        function imageDone() {
            this.container.style.backgroundImage = 'url('+this.src+')';
            this.container.className += ' show';
        };
    }

    var Layout = React.createClass({
        getInitialState: function() {
            return {
                event: {
                    showFilm: this.showFilm
                },
                mainFilm: null
            };
        },
        showFilm: function(item) {
            this.setState({mainFilm: item});
        },
        hideFilm: function() {
            this.setState({mainFilm: null});
            document.body.className = '';
        },
        render: function() {
            var mainFilm = '';

            if (this.state.mainFilm) {
                mainFilm = <MainFilm back={this.hideFilm} item={this.state.mainFilm} />
            }

            return (
                <div className="wrapper">
                    {mainFilm}
                    <ItemList url="http://hackathon.ngalochkin.dev/grid/limit/20/offset/0" event = {this.state.event.showFilm} />
                </div>
                )
        }
    });

    var ItemList = React.createClass({
        showMainFilm: function(item) {
            this.props.event(item);
        },
        componentWillMount: function() {
            this.compleateItems = []
            this.addItems();
        },
        getInitialState: function() {
            return {data: []}
        },
        itemsOffset: 0,
        itemsCount: 0,
        addItems : function(url) {
            var self = this;
            $.ajax({
                url: url||this.props.url,
                dataType: 'json',
                success: function(data) {
                    var newArray = self.state.data.concat(data);

                    if (data.length) {
                        self.itemsOffset = self.itemsCount + data.length;
                        self.itemsCount =  self.itemsOffset;
                    } else {
                        self.itemsCount = 0;
                        self.itemsOffset = 0;
                        self.addItems();
                    }

                    this.setState({data: newArray});
                }.bind(this),
                error: function(xhr, status, err) {
                    console.error(this.props.url, status, err.toString());
                }.bind(this)
            })
        },
        render: function() {
            var items = this.state.data.map(function(item) {
                return <Item
                id={item.id}
                engtitle={item.title_original}
                description={item.description}
                country={item.countries}
                bgImg={item.bg_image}
                clickEvent={this.showMainFilm}
                img={item.poster}
                title={item.title}
                year={item.year}
                />
            }.bind(this))

            return (
                <ul ref="list" className="b-list">
                    {items}
                </ul>
                )
        },
        maxDelphTouchMove: 0,
        componentDidMount: function() {
            var self = this;


            function pushReq() {
                var requestUrl = 'http://hackathon.ngalochkin.dev/grid/limit/6/offset/'+self.itemsOffset;
                self.addItems(requestUrl);
            }

            window.addEventListener('scroll', function() {
                if (window.pageYOffset+window.screen.height >= document.body.offsetHeight) {
                    pushReq()
                };
            })

            window.addEventListener('touchend', function() {
                self.maxDelphTouchMove++;

                if (self.maxDelphTpuchMove<10) {
                    pushReq();
                }
            })
        }
    });


    var MainFilm = React.createClass({
        render: function() {
            var item = this.props.item.props;
            return (
                <div id="mainFilm">
                    <div className="b-main-item-bg lazy-style" data-img={item.bgImg}></div>
                    <div className="b-main-item-content">
                        <div onClick={this.props.back} className="b-main-item-content-wrap">
                            <ul className="b-info">
                                <li className="b-info-row-title">
                                    {item.title}
                                    <div className="b-info-row-title-eng">{item.engtitle}</div>
                                </li>
                                <li className="b-info-row">
                                    <span className="b-info-row-caption">Год: </span>{item.year}
                                </li>
                                <li className="b-info-row">
                                    <span className="b-info-row-caption">Страна: </span>{item.country}
                                </li>
                                <li className="b-info-row">
                                    <span className="b-info-row-caption">Описание</span>
                                    <div className="b-info-row-description">{item.description}</div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div onClick={this.props.back} className="b-main-item-back">Назад</div>
                </div>
                )
        },
        componentDidMount: function() {
            var container = this.getDOMNode();

            setTimeout(function(){
                container.className += ' show';
                document.body.className = 'no-scroll';
                Lazy(container.childNodes[0]);
            }, 0)

        }
    });


    var Item = React.createClass({
        render: function() {
            return (
                <li onClick={this.filmClick} className="b-list-item">
                    <div data-img={this.props.img}  className ="b-list-item_image lazy-style"></div>
                    <a className = "b-list-item_link">{this.props.title}</a>
                    <div className="b-list-item_dummy">
                        <i className="icon"></i>
                    </div>
                </li>
                )
        },
        styleLazy: function(item) {
            Lazy(item);
        },
        filmClick: function() {
            this.props.clickEvent(this);
        },
        componentDidMount: function() {
            var item = this.getDOMNode().children[0];

            this.styleLazy(item);
        }
    })

    React.renderComponent(
        <Layout url="list.json" />,
        document.getElementById('wrapper')
    );
})

