define([
    "jquery",
    "react",
    "lazy-style",
    "wookmark"
], function($,React,Lazy) {
    var Layout = React.createClass({
        componentWillMount: function() {
            $.ajax({
                url: this.props.url,
                dataType: 'json',
                success: function(data) {
                    this.setState({data: data});
                }.bind(this),
                error: function(xhr, status, err) {
                    console.error(this.props.url, status, err.toString());
                }.bind(this)
            })
        },getInitialState: function() {
            return {data: [], event: {
                showFilm: this.showFilm
            },
            mainFilm: null};
        },
        showFilm: function(item) {
            this.setState({mainFilm: item});
        },
        hideFilm: function() {
            this.setState({mainFilm: null});
        },
        render: function() {
            var mainFilm = '';

            if (this.state.mainFilm) {
                mainFilm = <MainFilm back={this.hideFilm} item={this.state.mainFilm} />
            }

            return (
                <div className="wrapper">
                    {mainFilm}
                    <ItemList event = {this.state.event.showFilm} data={this.state.data} />
                </div>
                )
        }
    });

    var MainFilm = React.createClass({
        render: function() {
            return (
                <div id="mainFilm">
                    <div className="b-main-item-bg"></div>
                    <div className="b-main-item-content">
                        <div onClick={this.props.back} className="b-main-item-back">Назад</div>
                    </div>
                </div>
                )
        },
        back: function() {
//            this.setState
        },
        componentDidMount: function() {
            var container = this.getDOMNode();

            setTimeout(function(){
                container.className += ' show';
            }, 0)

        }
    });

    var ItemList = React.createClass({
        showMainFilm: function(item) {
            this.props.event(item);
        },
        render: function() {
            var count = 0,
                loveArray = [];

                items = this.props.data.map(function(item) {
                    var item = item.item;
                    count++;

                    if (item.loveComment) {
                        loveArray.push({
                            id: count,
                            comment: item.loveComment
                        })
                    }



                    return <Item id={count} clickEvent={this.showMainFilm} img={item.img} title={item.title} />
                }.bind(this))

            this.loveComments(loveArray);

            return (
                <div ref="list" className="b-list">
                {items}
                </div>
                )
        },
//        loveComments: function(love) {
//            window.addEventListener('scroll', function(e) {
//                console.log(window.pageYOffset);
//            })
////            console.log($(this.getDOMNode()));
//        },
        componentDidMount: function() {

        }
    });


    var Item = React.createClass({
        render: function() {
            return (
                <div onClick={this.filmClick} className="b-list-item">
                    <div data-img={this.props.img}  className = "b-list-item_image lazy-style"></div>
                    <a className = "b-list-item_link">{this.props.title}</a>
                </div>
                )
        },
        styleLazy: function(item) {
            loadImage(item);
            function loadImage(container) {
                var src = container.getAttribute('data-img'),
                    handlerImage = new Image();

                handlerImage.src = src;
                handlerImage.container = container;
                handlerImage.onload = imageDone;
            };

            function imageDone() {
                this.container.style.backgroundImage = 'url('+this.src+')';
                this.container.className += ' show';
            };
        },
        filmClick: function() {
            this.props.clickEvent(this);
        },
        componentDidMount: function() {
            var item = this.getDOMNode().children[0];

            this.styleLazy(item);
        }
    })

    React.renderComponent(
        <Layout url="list.json" />,
        document.getElementById('wrapper')
    );
})

