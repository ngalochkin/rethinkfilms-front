var rethink = require('rethinkdb');

var connection = null;
rethink.connect({host: 'localhost', port: 28015}, function (err, conn) {
    if (err) throw err;
    connection = conn;
});

exports.connection = connection;